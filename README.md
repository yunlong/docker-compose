## centos 下安装 docker-compose
参照 https://docs.docker.com/compose/install/

推荐方法：
```
curl -L "https://github.com/docker/compose/releases/download/1.8.1/docker-compose-$(uname -s)-$(uname -m)" > /usr/local/bin/docker-compose
```

赋予执行权限：
```
chmod +x /usr/local/bin/docker-compose
```

## 使用
1、下载对应的 yml 文件，并改名为 docker-compose.yml，执行如下命令即可：
```
docker-compose up -d
```
2、下载对应的 yml 文件，使用-f指定代替的compose文件，如下命令指定特定的 yml 文件：
```
docker-compose -f gogs.yml up -d
```